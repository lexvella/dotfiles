# Utilities to manage PowerShell Consoles
# Based on code from ConCFG: https://github.com/lukesampson/concfg/

Add-Type -TypeDefinition @'
using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace dotfiles {
    public class ShortcutManager {
        public static void ResetConsoleProperties(string path) {
            if (!System.IO.File.Exists(path)) { return; }

            var file = new ShellLink() as IPersistFile;
            if (file == null) { return; }

            file.Load(path, 2 /* STGM_READWRITE */);
            var data = (IShellLinkDataList) file;

            data.RemoveDataBlock( 0xA0000002 /* NT_CONSOLE_PROPS_SIG */);
            file.Save(path, true);

            Marshal.ReleaseComObject(data);
            Marshal.ReleaseComObject(file);
        }
    }

    [ComImport, Guid("00021401-0000-0000-C000-000000000046")]
    class ShellLink { }

    [ComImport, Guid("45e2b4ae-b1c3-11d0-b92f-00a0c90312e1"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    interface IShellLinkDataList {
        void AddDataBlock(IntPtr pDataBlock);
        void CopyDataBlock(uint dwSig, out IntPtr ppDataBlock);
        void RemoveDataBlock(uint dwSig);
        void GetFlags(out uint pdwFlags);
        void SetFlags(uint dwFlags);
    }
}
'@

function Verify-Elevated {
    # Get the ID and security principal of the current user account
    $myIdentity=[System.Security.Principal.WindowsIdentity]::GetCurrent()
    $myPrincipal=new-object System.Security.Principal.WindowsPrincipal($myIdentity)
    # Check to see if we are currently running "as Administrator"
    return $myPrincipal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
}

function Verify-PowershellShortcut {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string] $Path
    )

    if (!(Test-Path $Path -PathType Leaf)) { return $false }
    if ([System.IO.Path]::GetExtension($Path) -ne ".lnk") { return $false; }

    $shell = New-Object -COMObject WScript.Shell -Strict
    $shortcut = $shell.CreateShortcut("$(Resolve-Path $Path)")

    $result = ($shortcut.TargetPath -eq "$env:WINDIR\system32\WindowsPowerShell\v1.0\powershell.exe") -or `
      ($shortcut.TargetPath -eq "$env:WINDIR\syswow64\WindowsPowerShell\v1.0\powershell.exe")
    [Runtime.Interopservices.Marshal]::ReleaseComObject($shortcut) | Out-Null
    return $result
}

function Verify-BashShortcut {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string] $Path
    )

    if (!(Test-Path $Path -PathType Leaf)) { return $false }
    if ([System.IO.Path]::GetExtension($Path) -ne ".lnk") { return $false; }

    $shell = New-Object -COMObject WScript.Shell -Strict
    $shortcut = $shell.CreateShortcut("$(Resolve-Path $Path)")

    $result = ($shortcut.TargetPath -eq "$env:WINDIR\system32\bash.exe")
    [Runtime.Interopservices.Marshal]::ReleaseComObject($shortcut) | Out-Null
    return $result
}

function Reset-PowerShellShortcut {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string] $Path
    )

    if (!(Test-Path $Path)) { Return }

    if (Test-Path $Path -PathType Container) {
        Get-ChildItem $Path | ForEach {
            Reset-PowerShellShortcut $_.FullName
        }
        Return
    }

    if (Verify-PowershellShortcut $Path) {
        $filePath = Resolve-Path $Path

        try {
            [dotfiles.ShortcutManager]::ResetConsoleProperties($filePath)
            $shell = New-Object -COMObject WScript.Shell -Strict
            $shortcut = $shell.CreateShortcut("$(Resolve-Path $path)")
            $shortcut.Arguments = "-nologo"
            $shortcut.Save()
            [Runtime.Interopservices.Marshal]::ReleaseComObject($shortcut) | Out-Null
            [Runtime.Interopservices.Marshal]::ReleaseComObject($shell) | Out-Null
        }
        catch [UnauthorizedAccessException] {
            Write-Warning "warning: admin permission is required to remove console props from $path"
        }
    }
}

function Reset-BashShortcut {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string] $Path
    )

    if (!(Test-Path $Path)) { Return }

    if (Test-Path $Path -PathType Container) {
        Get-ChildItem $Path | ForEach {
            Reset-BashShortcut $_.FullName
        }
        Return
    }

    if (Verify-BashShortcut $Path) {
        $filePath = Resolve-Path $Path

        try {
            [dotfiles.ShortcutManager]::ResetConsoleProperties($filePath)
            $shell = New-Object -COMObject WScript.Shell -Strict
            $shortcut = $shell.CreateShortcut("$(Resolve-Path $path)")
            $shortcut.Save()
            [Runtime.Interopservices.Marshal]::ReleaseComObject($shortcut) | Out-Null
            [Runtime.Interopservices.Marshal]::ReleaseComObject($shell) | Out-Null
        }
        catch [UnauthorizedAccessException] {
            Write-Warning "warning: admin permission is required to remove console props from $path"
        }
    }
}

function Reset-AllPowerShellShortcuts {
    @(`
        "$ENV:ALLUSERSPROFILE\Microsoft\Windows\Start Menu\Programs",`
        "$ENV:USERPROFILE\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch\User Pinned\StartMenu",`
        "$ENV:USERPROFILE\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar",`
        "$ENV:USERPROFILE\AppData\Roaming\Microsoft\Windows\Start Menu\Programs"`
    ) | ForEach { Reset-PowerShellShortcut $_ }
}

function Reset-AllBashShortcuts {
    @(`
        "$ENV:ALLUSERSPROFILE\Microsoft\Windows\Start Menu\Programs",`
        "$ENV:USERPROFILE\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch\User Pinned\StartMenu",`
        "$ENV:USERPROFILE\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar",`
        "$ENV:USERPROFILE\AppData\Roaming\Microsoft\Windows\Start Menu\Programs"`
    ) | ForEach { Reset-BashShortcut $_ }
}

function Convert-ConsoleColor {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]$rgb
    )

    if ($rgb -notmatch '^#[\da-f]{6}$') {
        write-Error "Invalid color '$rgb' should be in RGB hex format, e.g. #000000"
        Return
    }
    $num = [Convert]::ToInt32($rgb.substring(1,6), 16)
    $bytes = [BitConverter]::GetBytes($num)
    [Array]::Reverse($bytes, 0, 3)
    return [BitConverter]::ToInt32($bytes, 0)
}

#$PSReadLineRegistry = Get-ItemProperty 'HKCU:\Console\PSReadLine' -ErrorAction SilentlyContinue
#Set-PSReadlineOption -TokenKind Comment   -ForegroundColor ($PSReadLineRegistry.CommentForeground  , "DarkGreen" -ne $null)[0]
#Set-PSReadlineOption -TokenKind Keyword   -ForegroundColor ($PSReadLineRegistry.KeywordForeground  , "Green"     -ne $null)[0]
#Set-PSReadlineOption -TokenKind String    -ForegroundColor ($PSReadLineRegistry.StringForeground   , "DarkCyan"  -ne $null)[0]
#Set-PSReadlineOption -TokenKind Operator  -ForegroundColor ($PSReadLineRegistry.OperatorForeground , "DarkGray"  -ne $null)[0]
#Set-PSReadlineOption -TokenKind Variable  -ForegroundColor ($PSReadLineRegistry.VariableForeground , "Green"     -ne $null)[0]
#Set-PSReadlineOption -TokenKind Command   -ForegroundColor ($PSReadLineRegistry.CommandForeground  , "Yellow"    -ne $null)[0]
#Set-PSReadlineOption -TokenKind Parameter -ForegroundColor ($PSReadLineRegistry.ParameterForeground, "DarkGray"  -ne $null)[0]
#Set-PSReadlineOption -TokenKind Type      -ForegroundColor ($PSReadLineRegistry.TypeForeground     , "Gray"      -ne $null)[0]
#Set-PSReadlineOption -TokenKind Number    -ForegroundColor ($PSReadLineRegistry.NumberForeground   , "White"     -ne $null)[0]
#Set-PSReadlineOption -TokenKind Member    -ForegroundColor ($PSReadLineRegistry.MemberForeground   , "Gray"      -ne $null)[0]
#Set-PSReadlineOption -TokenKind None      -ForegroundColor ($PSReadLineRegistry.NormalForeground   , "$([System.Console]::ForegroundColor)" -ne $null)[0]
#Set-PSReadlineOption -EmphasisForegroundColor              ($PSReadLineRegistry.EmphasisForeground , "Cyan" -ne $null)[0]
#Set-PSReadlineOption -ErrorForegroundColor                 ($PSReadLineRegistry.ErrorForeground    , "Red"  -ne $null)[0]
#Remove-Variable PSReadLineRegistry
#@(`
#"HKCU:\Console\%SystemRoot%_System32_bash.exe",`
#"HKCU:\Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe",`
#"HKCU:\Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe",`
#"HKCU:\Console\Windows PowerShell (x86)",`
#"HKCU:\Console\Windows PowerShell",`
#"HKCU:\Console"`
#) | ForEach {
#    If (!(Test-Path $_)) {
#        New-Item -path $_ -ItemType Folder | Out-Null
#    }
#
## Dimensions of window, in characters: 8-byte; 4b height, 4b width. Max: 0x7FFF7FFF (32767h x 32767w)
#Set-ItemProperty $_ "WindowSize"           0x002D0078 # 45h x 120w
## Dimensions of screen buffer in memory, in characters: 8-byte; 4b height, 4b width. Max: 0x7FFF7FFF (32767h x 32767w)
#Set-ItemProperty $_ "ScreenBufferSize"     0x0BB80078 # 3000h x 120w
## Percentage of Character Space for Cursor: 25: Small, 50: Medium, 100: Large
#Set-ItemProperty $_ "CursorSize"           100
## Name of display font
##Set-ItemProperty $_ "FaceName"             "Source Code Pro"
## Font Family: Raster: 0, TrueType: 54
#Set-ItemProperty $_ "FontFamily"           54
## Dimensions of font character in pixels, not Points: 8-byte; 4b height, 4b width. 0: Auto
#Set-ItemProperty $_ "FontSize"             0x00110000 # 17px height x auto width
## Boldness of font: Raster=(Normal: 0, Bold: 1), TrueType=(100-900, Normal: 400)
#Set-ItemProperty $_ "FontWeight"           400
## Number of commands in history buffer
#Set-ItemProperty $_ "HistoryBufferSize"    50
## Discard duplicate commands
#Set-ItemProperty $_ "HistoryNoDup"         1
## Typing Mode: Overtype: 0, Insert: 1
#Set-ItemProperty $_ "InsertMode"           1
## Enable Copy/Paste using Mouse
#Set-ItemProperty $_ "QuickEdit"            1
## Background and Foreground Colors for Window: 2-byte; 1b background, 1b foreground; Color: 0-F
#Set-ItemProperty $_ "ScreenColors"         0x0F
## Background and Foreground Colors for Popup Window: 2-byte; 1b background, 1b foreground; Color: 0-F
#Set-ItemProperty $_ "PopupColors"          0xF0
## Adjust opacity between 30% and 100%: 0x4C to 0xFF -or- 76 to 255
#Set-ItemProperty $_ "WindowAlpha"          0xF2
#
## The 16 colors in the Console color well (Persisted values are in BGR).
## Theme: Jellybeans
#Set-ItemProperty $_ "ColorTable00"         $(Convert-ConsoleColor "#151515") # Black (0)
#Set-ItemProperty $_ "ColorTable01"         $(Convert-ConsoleColor "#8197bf") # DarkBlue (1)
#Set-ItemProperty $_ "ColorTable02"         $(Convert-ConsoleColor "#437019") # DarkGreen (2)
#Set-ItemProperty $_ "ColorTable03"         $(Convert-ConsoleColor "#556779") # DarkCyan (3)
#Set-ItemProperty $_ "ColorTable04"         $(Convert-ConsoleColor "#902020") # DarkRed (4)
#Set-ItemProperty $_ "ColorTable05"         $(Convert-ConsoleColor "#540063") # DarkMagenta (5)
#Set-ItemProperty $_ "ColorTable06"         $(Convert-ConsoleColor "#dad085") # DarkYellow (6)
#Set-ItemProperty $_ "ColorTable07"         $(Convert-ConsoleColor "#888888") # Gray (7)
#Set-ItemProperty $_ "ColorTable08"         $(Convert-ConsoleColor "#606060") # DarkGray (8)
#Set-ItemProperty $_ "ColorTable09"         $(Convert-ConsoleColor "#7697d6") # Blue (9)
#Set-ItemProperty $_ "ColorTable10"         $(Convert-ConsoleColor "#99ad6a") # Green (A)
#Set-ItemProperty $_ "ColorTable11"         $(Convert-ConsoleColor "#c6b6ee") # Cyan (B)
#Set-ItemProperty $_ "ColorTable12"         $(Convert-ConsoleColor "#cf6a4c") # Red (C)
#Set-ItemProperty $_ "ColorTable13"         $(Convert-ConsoleColor "#f0a0c0") # Magenta (D)
#Set-ItemProperty $_ "ColorTable14"         $(Convert-ConsoleColor "#fad07a") # Yellow (E)
#Set-ItemProperty $_ "ColorTable15"         $(Convert-ConsoleColor "#e8e8d3") # White (F)
#}